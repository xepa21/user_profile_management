@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Password</div>

                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

                {!! Form::model($user, ['method' => 'PUT', 'route' => ['users.update_profile', $user->id], 'files' => true, 'id' => 'saveUser']) !!}

                <div class="panel panel-default">
                    <div class="panel-heading">
                       <h3> Profile</h3>
                    </div>
                    
                    <div class="panel-body">
                        
                           <div class="col-xs-6 form-group">
                                <label>Profile Image</label>
                                @if($user->profile_image != '')
                                <img src="{{url('public/uploads/'.$user->profile_image)}}" height="50px" width="50px">
                                @else
                                <img src="{{url('public/uploads/no_image.jpg')}}" height="50px" width="50px">                                
                                @endif
                                {!! Form::file('profile_image', old('profile_image'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('profile_image'))
                                    <p class="help-block">
                                        {{ $errors->first('profile_image') }}
                                    </p>
                                @endif
                            </div>
                        <div class="row">                             

                            {!! Form::submit('Update', ['class' => 'btn btn-danger']) !!}    
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        
    });
    
</script>
@endsection
