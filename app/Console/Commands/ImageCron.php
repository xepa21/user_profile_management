<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Auth;
use File;

class ImageCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For Deleting Old Images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d H:i:s');
        $currentDate = strtotime($date);
        $futureDate = $currentDate-(60*5);
        $formatDate = date("Y-m-d H:i:s", $futureDate);
        $userData = User::where('profileUpdated','<',$formatDate)->get();
        \Log::info($userData);
        if($userData) {
            foreach($userData as $k=>$v) {
                $profileImage = $v->profile_image;
                $image_path = public_path('uploads/'.$profileImage);  
                if(file_exists($image_path)) {
                    @unlink($image_path);
                    $updateUser = User::find($v->id);
                    $updateUser->profile_image = '';
                    $updateUser->save();
                    \Log::info("Cron is working fine!");
                }
                \Log::info($image_path);
            }
        }
        \Log::info('image:Cron Cummand Run successfully!');

    }
}
