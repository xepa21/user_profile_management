<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Collective\Html\Eloquent\FormAccessible;
use Hash;
use DB;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'first_name' => 'required|string|max:25',
            'last_name' => 'required|string|max:25',
            'mobile_number' => 'required|numeric',
            'profile_image' => 'max:2048|mimes:jpeg,jpg,png,gif',
            'dateOfBirth' => 'required'  
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $admin_user = User::find($id);
        $filename = $admin_user->profile_image;
        // dd($request);
        if($request->hasFile('profile_image')){
            $profile = $request->file('profile_image');
            $filename = time() . '.' . $profile->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $profile->move($destinationPath, $filename);
        }
        // dd($request->dateOfBirth);
        
        $admin_user->first_name = $request->first_name;
        $admin_user->last_name = $request->last_name;
        $admin_user->mobile_number = $request->mobile_number;
        $admin_user->profile_image = $filename;
        $admin_user->dateOfBirth = $request->dateOfBirth;
        $admin_user->save();
        // $hashed = Hash::make($password);

        return redirect()->back()->with('success', 'Profile Updated Successfully');
    }

    public function update_password() {
        $getuser = Auth::user()->id;
        $user = User::find($getuser);
        return view('user.update_password', compact('user'));
    }

    public function updatePassword(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'old_password'     => 'required',
            'new_password'     => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $data = $request->all();
     
        $user = User::find(auth()->user()->id);
        if(!Hash::check($data['old_password'], $user->password)){
             return back()
                        ->with('error','The specified password does not match the database password');
        }else{
            $getUser = User::find($id);
            $getUser->password = Hash::make($request->new_password);
            $getUser->save();
             return back()->with('success','Password Updated Successfully');
        }
    }

    public function update_profile_picture() {
        $getuser = Auth::user()->id;
        $user = User::find($getuser);
        return view('user.update_profile', compact('user'));
    }

    public function update_profile(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'profile_image'     => 'required|mimes:jpeg,jpg,png,gif',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $getUser = User::find($id);

        $filename = $getUser->profile_image;
        if($request->hasFile('profile_image')){
            $profile = $request->file('profile_image');
            $filename = time() . '.' . $profile->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $profile->move($destinationPath, $filename);
        }
        $getUser->profile_image = $filename;
        $getUser->profileUpdated = date('Y-m-d H:i:s');
        $getUser->save();
        return back()->with('success','Profile Updated Successfully');

    }
}


