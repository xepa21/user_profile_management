<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'first_name' => ['required', 'string', 'max:25'],
            'last_name' => ['required', 'string', 'max:25'],
            'password' => ['required', 'string', 'min:8'],
            'mobile_number' => ['required', 'numeric'],
            'profile_image' => ['required', 'max:2048', 'mimes:jpeg,jpg,png,gif'],
            'dateOfBirth' => ['required']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $request = app('request');
        // dd($request);
        $filename = '';
        if($request->hasfile('profile_image')){
            // dd('dff');
            $profile = $request->file('profile_image');
            $filename = time() . '.' . $profile->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $profile->move($destinationPath, $filename);
        }

        return User::create([
        'email' => $data['email'],
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],        
        'password' => Hash::make($data['password']),
        'mobile_number' => $data['mobile_number'],
        'profile_image' => $filename,
        'profileUpdated' => date('Y-m-d H:i:s'),
        'dateOfBirth' => $data['dateOfBirth'],
        ]);
        // return redirect('home');

    }    
}
