@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    <h2>You are logged in!</h2>

                {!! Form::model($user, ['method' => 'PUT', 'route' => ['users.update', $user->id], 'files' => true, 'id' => 'saveUser']) !!}

                <div class="panel panel-default">
                    <div class="panel-heading">
                       <h3> Profile</h3>
                    </div>
                    
                    <div class="panel-body">
                        
                            <div class="col-xs-6 form-group">
                                <label>First Name</label>
                                {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => '', 'readonly' => 'true']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('first_name'))
                                    <p class="help-block">
                                        {{ $errors->first('first_name') }}
                                    </p>
                                @endif
                            </div>

                            <div class="col-xs-6 form-group">
                                <label>Last Name</label>
                                {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => '', 'readonly' => 'true']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('last_name'))
                                    <p class="help-block">
                                        {{ $errors->first('last_name') }}
                                    </p>
                                @endif
                            </div>

                            <div class="col-xs-6 form-group">
                                <label>Email</label>
                                {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'readonly' => 'true']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('email'))
                                    <p class="help-block">
                                        {{ $errors->first('email') }}
                                    </p>
                                @endif
                            </div>
                        
                            <div class="col-xs-6 form-group">
                                <label>Mobile Number</label>
                                {!! Form::number('mobile_number', old('mobile_number'), ['class' => 'form-control', 'placeholder' => '', 'readonly' => 'true']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('mobile_number'))
                                    <p class="help-block">
                                        {{ $errors->first('mobile_number') }}
                                    </p>
                                @endif
                            </div>
                        
                            <div class="col-xs-6 form-group">
                                <label>Profile Image</label>
                                @if($user->profile_image != '')
                                <img src="{{url('public/uploads/'.$user->profile_image)}}" height="50px" width="50px">
                                @else
                                <img src="{{url('public/uploads/no_image.jpg')}}" height="50px" width="50px">                                
                                @endif
                                {!! Form::file('profile_image', old('profile_image'), ['class' => 'form-control', 'placeholder' => '', 'disabled', 'accept' => 'images/*']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('profile_image'))
                                    <p class="help-block">
                                        {{ $errors->first('profile_image') }}
                                    </p>
                                @endif
                            </div>
                            <div class="col-xs-6 form-group">
                                <label>Date of Birth</label>
                                <?php 
                                $dob = date('Y-m-d', strtotime($user->dateOfBirth));
                                $maxDate = date('Y-m-d'); 
                                ?>                                
                                {!! Form::date('dateOfBirth', $dob, ['class' => 'form-control', 'placeholder' => '', 'readonly' => 'true', 'id' => 'datepicker','max'=> $maxDate]) !!}
                                <p class="help-block"></p>
                                @if($errors->has('dateOfBirth'))
                                    <p class="help-block">
                                        {{ $errors->first('dateOfBirth') }}
                                    </p>
                                @endif
                            </div>
                        
                        <div class="row">
                            {!! Form::button('Update Profile', ['class' => 'btn btn-success', 'onclick' => 'updateProfile()', 'style' => 'margin-right:15px']) !!}    

                            {!! Form::button('Save', ['class' => 'btn btn-danger', 'onclick' => 'saveProfile()']) !!}    
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    $(document).ready(function() {
        $( "#datepicker" ).datepicker();
    });
    function updateProfile() {
            $(':input').prop('readonly', false);
    }

    function saveProfile() {
        if ( $('input').is('[readonly]') ) { 
            alert('Please click on Update Profile Button First to change Profile');
        } else {
          $("#saveUser").submit();  
        }
    }
</script>
@endsection
