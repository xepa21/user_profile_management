@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Password</div>

                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

                {!! Form::model($user, ['method' => 'PUT', 'route' => ['users.update_password', $user->id], 'files' => true, 'id' => 'saveUser']) !!}

                <div class="panel panel-default">
                    <div class="panel-heading">
                       <h3> Profile</h3>
                    </div>
                    
                    <div class="panel-body">
                        
                           <div class="col-xs-6 form-group">
                                <label>Old Password</label>
                               <input type="password" name="old_password" class="form-control" required="">
                                <p class="help-block"></p>
                                @if($errors->has('old_password'))
                                    <p class="help-block">
                                        {{ $errors->first('old_password') }}
                                    </p>
                                @endif
                            </div>
                        
                            <div class="col-xs-6 form-group">
                                <label>New Password</label>
                                <input type="password" name="new_password" class="form-control" required="">
                                <p class="help-block"></p>
                                @if($errors->has('new_password'))
                                    <p class="help-block">
                                        {{ $errors->first('new_password') }}
                                    </p>
                                @endif
                            </div>

                            <div class="col-xs-6 form-group">
                                <label>Confirm Password</label>
                                <input type="password" name="confirm_password" class="form-control" required="">
                                <p class="help-block"></p>
                                @if($errors->has('confirm_password'))
                                    <p class="help-block">
                                        {{ $errors->first('confirm_password') }}
                                    </p>
                                @endif
                            </div>
                        
                        <div class="row">                             

                            {!! Form::submit('Update', ['class' => 'btn btn-danger']) !!}    
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        
    });
    
</script>
@endsection
